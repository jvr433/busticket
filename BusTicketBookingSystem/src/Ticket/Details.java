package Ticket;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class Details extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Details frame = new Details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Details() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBusno = new JLabel("BusNo");
		lblBusno.setBounds(56, 44, 46, 14);
		contentPane.add(lblBusno);
		
		JLabel lblBustype = new JLabel("BusType");
		lblBustype.setBounds(56, 83, 59, 14);
		contentPane.add(lblBustype);
		
		JLabel lblSource = new JLabel("Source");
		lblSource.setBounds(56, 127, 46, 14);
		contentPane.add(lblSource);
		
		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setBounds(56, 168, 71, 14);
		contentPane.add(lblDestination);
		
		JLabel lblBusFare = new JLabel("Bus Fare");
		lblBusFare.setBounds(56, 206, 46, 14);
		contentPane.add(lblBusFare);
	
		textField = new JTextField();
		textField.setBounds(125, 41, 156, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(125, 80, 156, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(125, 124, 156, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(125, 165, 156, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(125, 203, 156, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String busfare,totalfare,numberoftickets;
				
				
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");
					PreparedStatement p=cn.prepareStatement("insert into Bus(busno,bustype,source,destination,busfare)values(?,?,?,?,?)");
					p.setString(1,textField.getText());
					p.setString(2,textField_1.getText());
					p.setString(3,textField_2.getText());
					p.setString(4,textField_3.getText());
					p.setString(5,textField_4.getText());
					int i=p.executeUpdate();
					if (i!=0){
						System.out.println("submited.........");
					}
					else
						System.out.println("not submitted");
				}
				catch(Exception ee){
					System.out.println(ee.getMessage());
				}
			}
		});	
		btnConfirm.setBounds(150, 234, 89, 31);
		contentPane.add(btnConfirm);
	}
}
