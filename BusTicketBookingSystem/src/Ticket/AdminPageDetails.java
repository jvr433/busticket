package Ticket;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.Font;
public class AdminPageDetails extends JFrame {

	private JPanel contentPane;
	//protected JComboBox cmbSrc;
	//protected JComboBox cmbDstn;
	protected JComboBox comboBox;
	protected JComboBox comboBox_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminPageDetails frame = new AdminPageDetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminPageDetails() {
		setTitle("AdminPageDetails");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		JLabel lblSrc = new JLabel("source");
		lblSrc.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSrc.setBounds(96, 63, 46, 14);
		contentPane.add(lblSrc);
		
		JLabel lblDstn = new JLabel("destination");
		lblDstn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDstn.setBounds(96, 103, 79, 14);
		contentPane.add(lblDstn);
		
		JButton btnInsert = new JButton("insert");
		btnInsert.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				Connection cn = DriverManager.getConnection
				("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");
				String query="insert into Searchbus(source,destination)values(?,?)";
				PreparedStatement pst=cn.prepareStatement(query);
				System.out.println("in insert");
				System.out.println(comboBox.getSelectedItem());
			    System.out.println(comboBox_1.getSelectedItem());
				pst.setString(1,(String) comboBox.getSelectedItem());
				pst.setString(2,(String) comboBox_1.getSelectedItem());
				
				pst.execute();
				JOptionPane.showMessageDialog(null, "Data Saved");
				pst.close();
				//rs.close();
			}
			
				catch (Exception e1) {
					e1.printStackTrace();
				}	
			}
		});
		btnInsert.setBounds(46, 170, 89, 23);
		contentPane.add(btnInsert);
		
		JButton btnUpdate = new JButton("update");
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn = DriverManager.getConnection
					("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");
					String query="Update searchbus set source=' "+comboBox.getSelectedItem()+" 'where source='"+comboBox_1.getSelectedItem()+"' ";
					PreparedStatement pst=cn.prepareStatement(query);
	//System.out.println(pst);
					pst.executeUpdate();
					JOptionPane.showMessageDialog(null, "Data Updated");
					pst.close();
				}
				catch (Exception e1) {
					e1.printStackTrace();
				
			}	
			}
		});
		btnUpdate.setBounds(145, 170, 89, 23);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("delete");
		btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn = DriverManager.getConnection
					("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");
					String query="delete from searchbus where source='"+comboBox.getSelectedItem()+"'";
					PreparedStatement pst=cn.prepareStatement(query);
					pst.execute();
					JOptionPane.showMessageDialog(null, "Data deleted");
					pst.close();
				}
				catch (Exception e1) {
					e1.printStackTrace();
				
			}	
			}
		});
		btnDelete.setBounds(250, 170, 89, 23);
		contentPane.add(btnDelete);
		
		 comboBox = new JComboBox();
		comboBox.setBounds(220, 62, 89, 20);
	contentPane.add(comboBox);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setBounds(220, 102, 89, 20);
		contentPane.add(comboBox_1);
		
		JButton btnView = new JButton("view");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewBusDetails s= new ViewBusDetails();
			setVisible(true);
			dispose();
			}
		
		});
		btnView.setBounds(321, 214, 89, 23);
		contentPane.add(btnView);
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");
			String sql="select * from searchbus";
			
			PreparedStatement p=connection.prepareStatement(sql);
			ResultSet results=p.executeQuery();
			java.sql.Statement statement=connection.createStatement();
			while(results.next()) {
				String item=results.getString("destination");
				comboBox_1.addItem(item);
				System.out.println(comboBox_1.getSelectedItem());

			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
		
	setVisible(true);
	try
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");
		String sql="select * from searchbus";
		
		PreparedStatement p=connection.prepareStatement(sql);
		ResultSet results=p.executeQuery();
		java.sql.Statement statement=connection.createStatement();
		while(results.next()) {
		String item=results.getString("source");
			comboBox.addItem(item);

		}
	}
	catch(Exception e) {
		System.out.println(e);
	}
}
	public static void main1(String[] args) {
	new AdminPageDetails();
	}
}

