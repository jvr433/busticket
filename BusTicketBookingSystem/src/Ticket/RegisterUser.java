package Ticket;
import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.Font;
public class RegisterUser {
	RegisterUser(){

		JFrame frame = new JFrame("Registration");
		frame.setSize(650, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		
		frame.getContentPane().add(panel);
		
		placeComponents(panel);
		
		frame.setVisible(true);
	}
	
	private static void placeComponents(JPanel panel) {
		
		panel.setLayout(null);
		
		JLabel LName = new JLabel("Name"); //creating Label
		LName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(LName);
        LName.setBounds(100,40,150,20);
		
		JLabel LAge = new JLabel("Age");
		LAge.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(LAge);
		LAge.setBounds(100,80,150,20);
		
		JLabel LMobileNumber = new JLabel("Mobile Number");
		LMobileNumber.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(LMobileNumber);
	LMobileNumber.setBounds(100,120,150,20);
		
		JLabel LUser_Name = new JLabel("User_Name");
		LUser_Name.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(LUser_Name);
		LUser_Name.setBounds(100,160,150,20);
		
		JLabel LPassword = new JLabel("Password");
		LPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel.add(LPassword);
		LPassword.setBounds(100,200,150,20);
		
		JTextField textName = new JTextField(); //create TextField
		panel.add(textName);
		textName.setColumns(20);
		textName.setBounds(350,40,150,20);
		
		JTextField textAge = new JTextField();
		panel.add(textAge);
		textAge.setColumns(20);
		textAge.setBounds(350,80,150,20);
		
		
		JTextField textMobileNumber = new JTextField();
		panel.add(textMobileNumber);
		textMobileNumber.setColumns(20);
		textMobileNumber.setBounds(350,120,150,20);
		
		
		JTextField textUserName = new JTextField();
		panel.add(textUserName);
		textUserName.setColumns(20);
		textUserName.setBounds(350,160,150,20);
		
		JTextField textPassword = new JTextField();
		panel.add(textPassword);
		textPassword.setColumns(20);
		textPassword.setBounds(350,200,150,20);
		
		
		JButton RegisterButton = new JButton("Register"); //CreateButton
		RegisterButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		RegisterButton.setBounds(240, 320, 120, 25);
		panel.add(RegisterButton);
		
		JButton BackButton = new JButton("Back");
		BackButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		BackButton.setBounds(240, 360, 120, 25);
		panel.add(BackButton);
		
		
		RegisterButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
			  
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn = DriverManager.getConnection
					("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");
					int count=textMobileNumber.getText().length();
					if(count!=10) {
						//Alert=1;
						JOptionPane.showMessageDialog(null, "Can you check your mobile No?");
					}
					else {
						//Alert=0;
					}
					String Query = "Insert into Register(Name, Age, MobileNumber, User_Name, Password)values(?,?,?,?,?)";
 					PreparedStatement st = cn.prepareStatement(Query);
					st.setString(1, textName.getText());
					st.setString(2, textAge.getText());
					st.setString(3, textMobileNumber.getText());
					st.setString(4, textUserName.getText());
					st.setString(5, textPassword.getText());
					st.executeUpdate();
					
					JOptionPane.showMessageDialog(null, "Register Successfully");
		
					cn.close();
					
				}
					
				
				catch(ClassNotFoundException e1)
				{
					e1.printStackTrace();
				}
				catch(SQLException s) { 
				System.out.println(s);
				}
				{
					
				}
			}  
			});
		BackButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				MainPage s1 = new MainPage();
				
			}
		
		});
	}
public static void main(String[] args) {
	new RegisterUser();
}
}
