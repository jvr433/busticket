package Ticket;
import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
public class SearchBus {
	private static JComboBox cmbSrc,cmbDstn;
	SearchBus() {
		JFrame frame = new JFrame("SEARCH BUS");
		frame.setSize(650, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		cmbSrc = new JComboBox();
		cmbSrc.setBounds(260, 100, 28, 20);
		cmbDstn = new JComboBox();
		cmbDstn.setBounds(260, 100, 28, 20);
		placeComponents(panel);
		frame.setVisible(true);
		}
		private static void placeComponents(JPanel panel) {

		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connection = DriverManager.getConnection
					("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");		
		
		
			String sql="select * from searchbus";
			
			PreparedStatement p=connection.prepareStatement(sql);
			ResultSet results=p.executeQuery();
			while(results.next()) {
				String source=results.getString("source");
				String destination = results.getString("destination");
				cmbSrc.addItem(source);
				cmbDstn.addItem(destination);
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
		panel.add(cmbSrc);
		panel.add(cmbDstn);
		
		
		JLabel LSource = new JLabel("Source :"); //creating Label
		panel.add(LSource);
		LSource.setBounds(100,100,150,20);
		
		JLabel LDestination = new JLabel("Destination :");
		panel.add(LDestination);
		LDestination.setBounds(100,150,150,20);
		
		JButton SearchButton = new JButton("Search"); //CreateButton
		SearchButton.setBounds(240, 320, 120, 25);
		panel.add(SearchButton);
		
		SearchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String str = (String) cmbSrc.getSelectedItem();
				String dstn =(String) cmbDstn.getSelectedItem();
				BusDetails s5 = new BusDetails(str,dstn);	
			    //s5.call();
			    }  
			});
		
		JButton BackButton = new JButton("Back");
		BackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login s3 = new Login();
			}
		});
		
		BackButton.setBounds(240, 360, 120, 25);
		panel.add(BackButton);
	    
	}
	public void call() {
		
	}
}
