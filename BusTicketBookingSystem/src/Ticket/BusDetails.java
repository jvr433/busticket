package Ticket;
import java.awt.EventQueue;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.EventQueue;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
public class BusDetails {
protected static final Date Chooser = null;
private JFrame frame;
	private JTextField textNoP;
	private JTextField textNumberOfTickets;
    int tktno;
    private JTextField textEmail;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BusDetails window = new BusDetails(null,null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
     /**
	 * Create the application.
	 */
	public BusDetails(String src ,String dstn)
	{
		Container picker1 = null ;
		initialize(src,dstn);
		frame.setVisible(true);
		
	}

	
	

	/**
	 * Initialize the contents of the frame.
	 * @param panel 
	 */
	private void initialize(String src,String destination) {
		frame = new JFrame("Bus Details");
		frame.setForeground(Color.YELLOW);
		frame.setSize(650, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		
		JLabel LBusNo = new JLabel("BusNO :");
		LBusNo.setBounds(80, 40, 150, 20);
		frame.getContentPane().add(LBusNo);
		
		JLabel LBustype = new JLabel("BusType :");
		LBustype.setBounds(80, 80, 150, 20);
		frame.getContentPane().add(LBustype);
	
		JLabel LSource = new JLabel("Source :");
		LSource.setBounds(80, 124, 150, 17);
		frame.getContentPane().add(LSource);
		
		JLabel LDestination = new JLabel("Destination :");
		LDestination.setBounds(80, 160, 150, 20);
		frame.getContentPane().add(LDestination);
		
		JLabel LBusFare = new JLabel("BusFare :");
		LBusFare.setBounds(80, 200, 150, 20);
		frame.getContentPane().add(LBusFare);
		
				
		JLabel L1BusNo = new JLabel("");
		L1BusNo.setBounds(350, 52, 200, 20);
		frame.getContentPane().add(L1BusNo);
		
		
		JLabel L1BusType = new JLabel("");
		L1BusType.setBounds(350, 80, 200, 20);
		frame.getContentPane().add(L1BusType);
		
		
		JLabel L1Source = new JLabel("");
		L1Source.setBounds(350, 124, 200, 20);
		frame.getContentPane().add(L1Source);
		
		
		JLabel L1Destination = new JLabel("");
		L1Destination.setBounds(350, 167, 200, 20);
		frame.getContentPane().add(L1Destination);
		
		JLabel L1BusFare = new JLabel("");
		L1BusFare.setBounds(350, 210, 200, 20);
		frame.getContentPane().add(L1BusFare);
		
		JButton ConfirmButton = new JButton("Confirm");
		ConfirmButton.setBounds(115, 499, 89, 23);
		frame.getContentPane().add(ConfirmButton);
		
		//JXDatePicker picker = new JXDatePicker();
		//picker.setDate(Calendar.getInstance().getTime());
		//picker.setFormats(new SimpleDateFormat("dd.MM.yyyy"));
		//picker.setBounds(350, 404, 150, 20);
		//frame.getContentPane().add(picker);
		try {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection cn = DriverManager.getConnection
				("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");		

		Statement st = cn.createStatement();
		String Sql = "Select busno,bustype,source,destination,busfare from bus where Source = '"+src +"' and Destination = '" +destination+"'";
		ResultSet rs = st.executeQuery(Sql);
		if(rs.next())
		{
			System.out.println("searched success");
			L1BusNo.setText(rs.getString(1));
			
			L1BusType.setText(rs.getString(2));
			L1Source.setText(rs.getString(3));
			L1Destination.setText(rs.getString(4));
			L1BusFare.setText(rs.getString(5));
			System.out.println("after searched success");
			
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Route not found");
		}
		
		cn.close();
		}
		
		
		catch(ClassNotFoundException e1)
		{
			e1.printStackTrace();
		}
		
		
		
		catch(SQLException s) { 
		System.out.println(s);
		}
		
		JLabel LCustomerDetail = new JLabel("Customer's details:");
		LCustomerDetail.setForeground(Color.BLUE);
		LCustomerDetail.setFont(new Font("Times New Roman", Font.BOLD, 26));
		LCustomerDetail.setBounds(205, 254, 244, 31);
		frame.getContentPane().add(LCustomerDetail);
		
		JLabel LNoP = new JLabel("Name Of The Passenger :");
		LNoP.setBounds(80, 296, 150, 20);
		frame.getContentPane().add(LNoP);
		
		JLabel LNumberOfTickets = new JLabel("Number Of Tickets :");
		LNumberOfTickets.setBounds(80, 360, 150, 20);
		frame.getContentPane().add(LNumberOfTickets);
		
		JLabel email = new JLabel("Email Of The Passenger :");
		email.setBounds(80, 320, 150, 20);
		frame.getContentPane().add(email);
		
		JLabel LDateOfJourney = new JLabel("Date Of Journey :");
		LDateOfJourney.setBounds(80, 404, 150, 20);
		frame.getContentPane().add(LDateOfJourney);
		
		
		
		JLabel lblNewLabel_3 = new JLabel("TotalFare");
		lblNewLabel_3.setBounds(80, 440, 150, 20);
		frame.getContentPane().add(lblNewLabel_3);
		
		textNoP = new JTextField();
		textNoP.setBounds(350, 296, 150, 20);
		frame.getContentPane().add(textNoP);
		textNoP.setColumns(10);
		
		textNumberOfTickets = new JTextField();
		textNumberOfTickets.setBounds(350, 360, 150, 20);
		frame.getContentPane().add(textNumberOfTickets);
		textNumberOfTickets.setColumns(10);
	
		JLabel L1TotalFare = new JLabel("");
		L1TotalFare.setBounds(350, 440, 100, 20);
		frame.getContentPane().add(L1TotalFare);
		
		//L1TotalFare.setText(Integer.toString(Integer.parseInt(textNumberOfTickets.getText()) *Integer.parseInt(L1BusFare.getText())));
	   
		
		ConfirmButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						String busfare,totalfare,numberoftickets;
					
						
						try {
							Class.forName("oracle.jdbc.driver.OracleDriver");
							Connection cn = DriverManager.getConnection
									("jdbc:oracle:thin:@localhost:1521:xe","system","vasundhara98");	
							String email=textEmail.getText();
							
							if(email.indexOf("@",0)<1) {
								JOptionPane.showMessageDialog(null, "Can you check your Email Address?");
							}
							else {
								
							}
							
							String Query = "Insert into BookingTicket"
									+ "(NameOfThePassenger, NumberOfTickets, "
									+ "DateOfJourney,TotalFare,"
									+ "TKTNO,email)"
									+ "values(?,?,?,?,S2.NEXTVAL,?)";
							PreparedStatement st = cn.prepareStatement(Query);
														st.setString(1, textNoP.getText());
														
														st.setString(2, textNumberOfTickets.getText());
													   // java.util.Date d1=Chooser;
													   // java.sql.Date d = new java.sql.Date(d1.getTime());
													   // st.setLong(3,d);
														st.setString(4, L1TotalFare.getText());
														st.setString(5, textEmail.getText());
                                                        st.executeUpdate();
							Statement st1 = cn.createStatement();
							String Sql = "Select S2.CURRVAL FROM DUAL";
							ResultSet rs = st.executeQuery(Sql);
							
							if(rs.next())
							  tktno = rs.getInt(1);
							JOptionPane.showMessageDialog(null, "BookingTicket Successfully");
						//	ShowTicket s7 = new ShowTicket(L1Source.getText(),L1Destination.getText(),tktno);
							cn.close();
						}
				
			
				catch(ClassNotFoundException e1)
				{
					e1.printStackTrace();
				}
				catch(SQLException s) { 
				System.out.println(s);
				}
				{
					
				}
				
								
					}
		});

		
		JButton CancelButton = new JButton("Cancel");
		CancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Login s3 = new Login();
			}
		});
		CancelButton.setBounds(237, 499, 89, 23);
		frame.getContentPane().add(CancelButton);
		
		JButton BackButton = new JButton("Back");
		BackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchBus s4 = new SearchBus();
			}
		});
		BackButton.setBounds(371, 499, 89, 23);
		frame.getContentPane().add(BackButton);
		
		JButton CheckButton = new JButton("Calculate");
		CheckButton.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent arg0) {
				
				L1TotalFare.setText(Integer.toString(Integer.parseInt(textNumberOfTickets.getText()) *Integer.parseInt(L1BusFare.getText())));
			}
		});
		CheckButton.setForeground(Color.BLACK);
		CheckButton.setBounds(513, 439, 90, 23);
		frame.getContentPane().add(CheckButton);
		
		textEmail = new JTextField();
		textEmail.setBounds(350, 320, 150, 20);
		frame.getContentPane().add(textEmail);
		textEmail.setColumns(10);
		}
	
		
	public void searchbus()
	{
		
	}
	public void SearchButton()
	{
		
	}
	
	
	public void call(){
	}
	
	public static void main1(String[] args) {
		new SearchBus();

	}	
}
